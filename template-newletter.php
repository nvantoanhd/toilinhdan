<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<?php
/**
 * Template Name: đăng ký nhận bản tin
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a page ('page' post_type) unless another page template overrules this one.
 * @link http://codex.wordpress.org/Pages
 *
 * @package WooFramework
 * @subpackage Template
 */
    get_header();
    global $woo_options;
?>
       
    <div id="content" class="page col-full">
    
        <?php woo_main_before(); ?>
        
        <section id="main" class="fullwidth">
           
        <?php
            if ( have_posts() ) { $count = 0;
                while ( have_posts() ) { the_post(); $count++;
        ?>                                                             
                <article <?php post_class(); ?>>
                    
                <header class="archive-header-full">                
                <h5 style="font-weight: normal;padding-left: 20px;"><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Trang chủ </a> 
                <?php _e( '', 'woothemes' ); ?>  / <?php single_cat_title( '', true ); ?>
                <?php the_title(); ?>
                </h5> 
                </header>
                    
                    <section class="entry">
                        <div class="dang-ky-nhan-ban-tin">
                            <?php dynamic_sidebar('dang-ky-nhan-ban-tin');?>
                        </div>
                        <?php the_content(); ?>
                    </section><!-- /.entry -->

                    <?php edit_post_link( __( '{ Edit }', 'woothemes' ), '<span class="small">', '</span>' ); ?>

                </article><!-- /.post -->
                                                    
            <?php
                    } // End WHILE Loop
                } else {
            ?>
                <article <?php post_class(); ?>>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'woothemes' ); ?></p>
                </article><!-- /.post -->
            <?php } ?>  
        
        </section><!-- /#main -->
        
        <?php //woo_main_after(); ?>
        
    </div><!-- /#content -->
        
<?php get_footer(); ?>

