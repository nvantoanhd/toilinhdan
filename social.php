<div class="social-button-container">

	<!-- Facebook -->
	<div class="social-fb">
		<div class="fb-like" data-href="<?php the_permalink(); ?>" data-send="false" data-width="150" layout="button_count" data-show-faces="false"></div>
	</div>

	<!-- Twitter -->
	<div class="social-twitter">
		<a href="https://twitter.com/share" class="twitter-share-button" data-via="cdils">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</div>

	<!-- Google Plus -->
	<div class="social-gplus">
		<g:plusone></g:plusone>
		<script type="text/javascript">
			window.___gcfg = {
				lang: 'en-US'
			};

			(function() {
				var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				po.src = 'https://apis.google.com/js/plusone.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			})();
		</script>
	</div>

	<!-- LinkedIN -->
	<div class="social-linkedin">
		<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
		<script type="IN/Share" data-url="<?php the_permalink(); ?>" data-counter="right" data-showzero="true"></script>
	</div>
</div>