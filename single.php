<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<?php
/**
 * Single Post Template
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a post ('post' post_type).
 * @link http://codex.wordpress.org/Post_Types#Post
 *
 * @package WooFramework
 * @subpackage Template
 */
	get_header();
	global $woo_options;
	
/**
 * The Variables
 *
 * Setup default variables, overriding them if the "Theme Options" have been saved.
 */
	
	$settings = array(
					'thumb_single' => 'false', 
					'single_w' => 787, 
					'single_h' => 300, 
					'thumb_single_align' => 'alignright'
					);
					
	$settings = woo_get_dynamic_values( $settings );
?>
       
    <div id="content" class="col-full">
    
    	<?php woo_main_before(); ?>
    	
		<section id="main" class="col-left">
		           
        <?php
        	if ( have_posts() ) { $count = 0;
        		while ( have_posts() ) { the_post(); $count++;
        ?>
			<article <?php post_class(); ?>>
				
				
				<section class="post-content-detail">

					<?php echo woo_embed( 'width=787' ); ?>
	                <?php if ( $settings['thumb_single'] == 'true' && ! woo_embed( '' ) ) { woo_image( 'width=' . $settings['single_w'] . '&height=' . $settings['single_h'] . '&class=thumbnail ' . $settings['thumb_single_align'] ); } ?>
	
	                <header>
	               	<header class="archive-header">
                
        			<h5 style="font-weight: normal; padding-left: 20px;"><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Trang chủ </a> 
                	<?php _e( '', 'woothemes' ); ?>  / <?php $categories = get_the_category(); foreach($categories as $category) { $cat_name = $category->name; if($cat_name != 'featured') echo '<a style="font-weight:normal;" href="'.get_category_link($category->term_id).'">'.$cat_name . '</a> '; } ?>
                	</h5>

        			</header>
	               	<h1><?php the_title(); ?></h1>

	                <aside class="meta-time">	
	                Cập nhật lúc: <?php the_time('g: i  - d/m/y') ?>				
					
					</aside>
	                
	                </header>
	                
	                
	                <section class="entry fix">
	                	<?php the_content(); ?>
	                	
	                	<?php the_tags( 'Từ khóa: ', ', ', '<br />' ); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) ); ?>
					</section>
													
				</section>
                                
            </article><!-- .post -->
				
            <?php wp_related_posts()?> 
            <h3 class="other-new-h3">Tin khác</h3>
            <?php echo delicious_recent_posts(); ?>

	        
            <?php
            	// Determine wether or not to display comments here, based on "Theme Options".
            	if ( isset( $woo_options['woo_comments'] ) && in_array( $woo_options['woo_comments'], array( 'post', 'both' ) ) ) {
            		comments_template();
            	}

				} // End WHILE Loop
			} else {
		?>
			<article <?php post_class(); ?>>
            	<p><?php _e( 'Sorry, no posts matched your criteria.', 'woothemes' ); ?></p>
			</article><!-- .post -->             
       	<?php } ?>  
        
		</section><!-- #main -->
		
		<?php woo_main_after(); ?>

        <?php get_sidebar(); ?>

    </div><!-- #content -->
		
<?php get_footer(); ?>