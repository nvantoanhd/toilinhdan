<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?>
<?php

/*-----------------------------------------------------------------------------------*/
/* Start WooThemes Functions - Please refrain from editing this section */
/*-----------------------------------------------------------------------------------*/

// Define the theme-specific key to be sent to PressTrends.
define( 'WOO_PRESSTRENDS_THEMEKEY', 'zdmv5lp26tfbp7jcwiw51ix9sj389e712' );

// WooFramework init
require_once ( get_template_directory() . '/functions/admin-init.php' );

/*-----------------------------------------------------------------------------------*/
/* Load the theme-specific files, with support for overriding via a child theme.
/*-----------------------------------------------------------------------------------*/

$includes = array(
				'includes/theme-options.php', 			// Options panel settings and custom settings
				'includes/theme-functions.php', 		// Custom theme functions
				'includes/theme-actions.php', 			// Theme actions & user defined hooks
				'includes/theme-comments.php', 			// Custom comments/pingback loop
				'includes/theme-js.php', 				// Load JavaScript via wp_enqueue_script
				'includes/sidebar-init.php', 			// Initialize widgetized areas
				'includes/theme-widgets.php',			// Theme widgets
				'includes/theme-install.php',			// Theme installation
				'includes/theme-woocommerce.php',		// WooCommerce options
				'includes/theme-plugin-integrations.php'	// Plugin integrations
				);

// Allow child themes/plugins to add widgets to be loaded.
$includes = apply_filters( 'woo_includes', $includes );

foreach ( $includes as $i ) {
	locate_template( $i, true );
}

/*-----------------------------------------------------------------------------------*/
/* You can add custom functions below */
/*-----------------------------------------------------------------------------------*/
register_sidebar(array(
    'name' => 'footer-block',
    'id' => 'footer-block',
    'description' => 'Khu vực sidebar hiển thị trên menu',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h1 class="widget-title">',
    'after_title' => '</h1>'
));
register_sidebar(array(
    'name' => 'logo-footer-block',
    'id' => 'logo-footer-block',
    'description' => 'Khu vực sidebar hiển thị trên menu',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h1 class="widget-title">',
    'after_title' => '</h1>'
));

register_sidebar(array(
    'name' => 'sidebar-right-home',
    'id' => 'sidebar-right-home',
    'description' => 'Khu vực sidebar hiển thị trên menu',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h1 class="widget-title">',
    'after_title' => '</h1>'
));

register_sidebar(array(
    'name' => 'content-left-home',
    'id' => 'content-left-home',
    'description' => 'Khu vực sidebar hiển thị trên menu',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h2 class="widget-title">',
    'after_title' => '</h2>'
));

register_sidebar(array(
    'name' => 'content-left-home-tu-van',
    'id' => 'content-left-home-tu-van',
    'description' => 'Khu vực sidebar hiển thị trên menu',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h2 class="widget-title">',
    'after_title' => '</h2>'
));

register_sidebar(array(
    'name' => 'suckhoe-content-left-home',
    'id' => 'suckhoe-content-left-home',
    'description' => 'Khu vực sidebar hiển thị trên menu',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h2 class="widget-title">',
    'after_title' => '</h2>'
));

register_sidebar(array(
    'name' => 'dang-ky-nhan-ban-tin',
    'id' => 'dang-ky-nhan-ban-tin',
    'description' => 'Khu vực sidebar hiển thị trên menu',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h1 class="widget-title">',
    'after_title' => '</h1>'
));

add_theme_support( 'post-thumbnails' );

add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart' );

function new_excerpt_length($length) {
    return 24;
}
add_filter('excerpt_length', 'new_excerpt_length');

function new_excerpt_more( $more ) {
    return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('... Đọc tiếp', 'your-text-domain') . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

?>
<ul>
<?php
function delicious_recent_posts() {
    $del_recent_posts = new WP_Query();
    $del_recent_posts->query('showposts=3');
        while ($del_recent_posts->have_posts()) : $del_recent_posts->the_post(); ?>
            <li class="other-new">
                <a href="<?php esc_url(the_permalink()); ?>">
                    <?php the_post_thumbnail('delicious-recent-thumbnails'); ?>
                </a>
                <h4>
                    <a href="<?php esc_url(the_permalink()); ?>">
                        <?php esc_html(the_title()); ?>
                   </a>
                </h4>
            </li>
        <?php endwhile;
    wp_reset_postdata();
}
?>
</ul>
<?php


/** Add Social Sharing Links on Single Posts **/


function include_social() {
    if ( is_single() )
require(CHILD_DIR.'/social.php');
}







/*-----------------------------------------------------------------------------------*/
/* Don't add any code below here or the sky will fall down */
/*-----------------------------------------------------------------------------------*/
?>