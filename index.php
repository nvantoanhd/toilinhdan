<?php
// File Security Check
if ( ! function_exists( 'wp' ) && ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?><?php
/**
 * Index Template
 *
 * Here we setup all logic and XHTML that is required for the index template, used as both the homepage
 * and as a fallback template, if a more appropriate template file doesn't exist for a specific context.
 *
 * @package WooFramework
 * @subpackage Template
 */
	get_header();
	global $woo_options;
	
?>
	

    <?php if ( $woo_options[ 'woo_homepage_banner' ] == "true" ) { ?>
    	
    	<div class="homepage-banner">
    		<?php
				if ( $woo_options[ 'woo_homepage_banner' ] == "true" ) { $banner = $woo_options['woo_homepage_banner_path']; }
				if ( $woo_options[ 'woo_homepage_banner' ] == "true" && is_ssl() ) { $banner = preg_replace("/^http:/", "https:", $woo_options['woo_homepage_banner_path']); }
			?>
			    <img src="<?php echo $banner; ?>" alt="" />
    		<h1><span><?php echo $woo_options['woo_homepage_banner_headline']; ?></span></h1>
    		<div class="description"><?php echo wpautop($woo_options['woo_homepage_banner_standfirst']); ?></div>
    	</div>
    	
    <?php } ?>
    
    <div id="content" class="col-full <?php if ( $woo_options[ 'woo_homepage_banner' ] == "true" ) echo 'with-banner'; ?> <?php if ( $woo_options[ 'woo_homepage_sidebar' ] == "false" ) echo 'no-sidebar'; ?>">
    
    
    	<?php woo_main_before(); ?>
    
		<section id="main" class="col-left">  

		<div class="slider-home-top">
		<?php echo do_shortcode("[huge_it_slider id='1']"); ?>
		</div>

		
		<div class="suckhoe-content-left-home">

		<?php dynamic_sidebar('suckhoe-content-left-home');?>
		</div>
		
		<div class="content-left-home">
		<?php dynamic_sidebar('content-left-home');?>
		</div>
		
		<div class="content-left-home-tu-van">
		<?php dynamic_sidebar('content-left-home-tu-van');?>
		</div>
		                
		</section><!-- /#main -->
		<div class = "sidebar-right-home">

		<?php dynamic_sidebar('sidebar-right-home');?>
		
		<?php woo_main_after(); ?>

        </div>

    </div><!-- /#content -->
		
<?php get_footer(); ?>